from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


def redirect_to_home(request):
    return redirect("home")


urlpatterns = [
    path("accounts/", include("accounts.urls")),
    path("", redirect_to_home),
    path("receipts/", include("receipts.urls")),
    path("admin/", admin.site.urls),
]
