from django.urls import path
from receipts.views import (
    list_receipts,
    create_receipt,
    list_accounts,
    list_category,
    create_account,
    create_category,
)


urlpatterns = [
    path("create/", create_receipt, name="create_receipt"),
    path("", list_receipts, name="home"),
    path("accounts/", list_accounts, name="account_list"),
    path("categories/", list_category, name="category_list"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_category, name="create_category"),
]
