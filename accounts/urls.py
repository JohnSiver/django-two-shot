from django.urls import path
from accounts.views import login_user, user_logout, signup


urlpatterns = [
    path("logout/", user_logout, name="logout"),
    path("login/", login_user, name="login"),
    path("signup/", signup, name="signup"),
]
